const Koa = require('koa');
const Router = require('koa-router');
const jwt = require('jsonwebtoken');
const cors = require('@koa/cors');

const app = new Koa();
const router = new Router({
	prefix: '/api'
});
const secret = 'secret';

// fake DB
let users = [
	{
		username: 'nguyen',
		password: '123456',
		authorization: '',
	},
	{
		username: 'cong',
		password: '654321',
		authorization: '',
	},
	{
		username: 'duc',
		password: '111111',
		authorization: '',
	},
];

router.post('/login', ctx => {
	if (ctx.request.body.username && ctx.request.body.password) {
		let username = ctx.request.body.username;
		let password = ctx.request.body.password;
		users.some((user, userId) => {		
			if (user.username === username && user.password === password) {
				let token = jwt.sign({ 'user_id': userId }, secret, { expiresIn: '30s' });
				Object.assign(user, { 'authorization': 'Bearer ' + token });
				ctx.status = 200;
				ctx.body = user;
				return true;
			} else {
				ctx.status = 401;
				ctx.body = { 'name': 'LoginError', 'message': "The entered account is invalid" };
			}
		});
	}
}); 

router.post('/check-bearer-token', ctx => {
	if (ctx.request.header.authorization && ctx.request.header.authorization.split(' ')[0] === 'Bearer') {
        let token = ctx.request.header.authorization.split(' ')[1];
        jwt.verify(token, secret, function(err, decoded) {
			if (err) {
				ctx.status = 401;
				ctx.body = err;
			} else {
				ctx.status = 200;
				ctx.body = decoded;
			}
		});
    }	
});

app
	.use(cors({
		origin: '*'
    }))
	.use(require('koa-body')())
	.use(router.routes())
	.use(router.allowedMethods());

app.listen(3000);