# loginapi

## HOW TO RUN THE API?
1. Install the dependencies:
*npm install*
2. Run **api.js** file:
*node api.js*



## HOW TO RUN THE EXAMPLE ON LOCALHOST?
1. Open **index.html** file in any browser (Chrome is recommended).
2. Login with one of the following sample accounts:
	- Username: 'nguyen', Password: '123456'
	- Username: 'cong', Password: '654321'
	- Username: 'duc', Password: '111111'
3. Now it generates an access token for the user, the token will last for 30 seconds.
4. You can click the "View Access Token Info" button to check the token's information.



## API DOCUMENTATION:
### LOGIN
* URL: /api/login
* Method: POST
* Headers: None
* URL Params: None
* Data Params:
	- Required: username=[alphanumeric], password=[alphanumeric]
* Success Response: 
	- Code: Success (200 OK)
	- Content: {
		username: 'some_un',
		password: 'some_pw',
		access_token: 'some_token'
	}
* Error Response:
	- Code: Unauthorized (401)
	- Content: {
		name: 'error_name',
		message: 'error_msg'
	}

### CHECK BEARER TOKEN
* URL: /api/check-bearer-token
* Method: POST
* Headers: 
	- Required: Authorization=Bearer <token>
* URL Params: None
* Data Params: None
* Success Response: 
	- Code: Success (200 OK)
	- Content: {
		user_id: 'some_user_id',
		iat: 'some_iat',
		exp: 'some_exp'
	}
* Error Response:
	- Code: Unauthorized (401)
	- Content: {
		name: 'error_name',
		message: 'error_msg'
	}